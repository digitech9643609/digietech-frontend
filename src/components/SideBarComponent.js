import Link from "next/link";
import CallCenterIcon from "./icon/CallCenterIcon";
import ShieldIcon from "./icon/ShieldIcon";
import Image from "next/image";

export default function SideBarComponent({}) {
    return (
        <div className="w-full h-auto flex flex-col gap-4">
            <div className="w-full h-auto flex flex-col gap-3 justify-center">
                <div className="w-full h-auto">
                    <Image
                        alt="image"
                        src={"/logo/MLBB_LOGO.jpg"}
                        width={432}
                        height={200}
                    />
                </div>
                <h1 className="text-lg text-white font-semibold capitalize">
                    Mobile Legends: Bang bang
                </h1>

                <div className="w-full h-auto flex items-center justify-start gap-3">
                    <button className="w-40 h-8 px-4 flex items-center justify-start gap-1 text-left bg-slate-100 rounded-full text-[10px] font-semibold text-[#45044a]">
                        <ShieldIcon
                            width={8}
                            height={8}
                            color={"text-[#45044a]"}
                        />
                        Pembayaran yang aman
                    </button>

                    <button className="w-40 h-8 px-4 flex items-center justify-start gap-1 text-left bg-slate-100 rounded-full text-[10px] font-semibold text-[#45044a]">
                        <CallCenterIcon
                            width={8}
                            height={8}
                            color={"text-[#45044a]"}
                        />
                        layanan pelanggan 24 jam
                    </button>
                </div>
            </div>

            <div className="w-full h-auto flex flex-col gap-6">
                <p className="text-white text-sm font-light">
                    Digitech shop menawarkan top up Mobile Legends yang mudah,
                    aman, dan instan.
                </p>

                <p className="text-white text-sm font-light">
                    Pembayaran tersedia melalui pulsa (Telkomsel, Indosat, Tri,
                    XL, Smartfren), Codacash, QRIS, GoPay, OVO, DANA, ShopeePay,
                    LinkAja, Krevido, Alfamart,Indomaret, DOKU, Bank Transfer
                    and Card Payments.
                </p>

                <p className="text-white text-sm font-light">
                    Top up ML Diamond, Twilight Pass and Weekly Pass hanya dalam
                    hitungan detik! Cukup masukan User ID dan Zone ID MLBB Anda,
                    pilih jumlah Diamond yang Anda inginkan, selesaikan
                    pembayaran, dan Diamond akan secara langsung ditambahkan ke
                    akun Mobile Legends Anda.
                </p>

                <p className="text-white text-sm font-light">
                    Digitech shop menunjukkan jumlah diamonds dengan format
                    berikut: Total Diamonds = (jumlah normal + Bonus), dimana
                    Total adalah jumlah akhir diamond yang akan diterima
                    customer in-game. (Hanya non-bonus diamond yang berlaku
                    untuk dihitung saat topup events.)
                </p>

                <p className="text-white text-sm font-light">
                    <Link
                        href="https://Digitechshop.com/"
                        className="text-yellow-500"
                    >
                        Login ke Digitech shop akunmu
                    </Link>{" "}
                    dan dapatkan akses ke promo Mobile Legends dan event
                    lainnya. Belum punya akun Digitech shop?{" "}
                    <Link
                        href="https://Digitechshop.com/"
                        className="text-yellow-500"
                    >
                        Daftar sekarang!
                    </Link>
                </p>
            </div>

            <div className="w-full h-auto flex flex-col gap-2">
                <span className="text-white text-sm">
                    Unduh Mobile Legends: Bang Bang sekarang!
                </span>

                <Image
                    alt="image"
                    src={"/logo/store-logo.png"}
                    width={200}
                    height={100}
                />
            </div>
        </div>
    );
}
