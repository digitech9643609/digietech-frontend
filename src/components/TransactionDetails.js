import { useGetTransactionByIdAPI } from "@/context/API/transactionAPI";
import { formatPriceToIDR } from "@/utils/formatIDRCurrency";
import transactionStatus from "@/utils/transactionStatus";
import { useSearchParams } from "next/navigation";
import { useEffect, useState } from "react";

export default function TransactionDetails({ paymentHandler, goBackHandler }) {
    const searchParams = useSearchParams();
    const transaction_id = searchParams.get("order_id");

    const {
        response,
        data: transactionData,
        transactionLoading,
        transactionError,
    } = useGetTransactionByIdAPI(transaction_id);

    return (
        <>
            {transactionLoading || transactionError || !transactionData ? (
                <div className="w-full h-40 flex items-center justify-center rounded-lg bg-slate-100">
                    <span
                        className={`text-sm text-[#45044a] font-semibold ${
                            transactionLoading ? "animate-pulse" : ""
                        }`}
                    >
                        {transactionLoading
                            ? "loading..."
                            : transactionError
                            ? "oops, failed to get the transaction"
                            : !transactionData
                            ? "transaction not found 😕"
                            : null}
                    </span>
                </div>
            ) : transactionData ? (
                <>
                    <div className="w-full h-auto grid grid-cols-2 gap-3 my-4">
                        <div className="h-full flex flex-col gap-4">
                            <div className="w-full h-auto">
                                <h1 className="text-sm -mb-1 font-light text-[#45044a]">
                                    Item :{" "}
                                </h1>
                                <span className="text-sm font-semibold text-[#45044a]">
                                    1 x Mobile Legends: Bang Bang |{" "}
                                    {
                                        transactionData.transaction_items
                                            .products.name
                                    }
                                </span>
                            </div>

                            <div className="w-full h-auto">
                                <h1 className="text-sm -mb-1 font-light text-[#45044a]">
                                    ID transaksi :{" "}
                                </h1>
                                <span className="text-sm font-semibold text-[#45044a]">
                                    {transactionData.transaction_items.TIid}
                                </span>
                            </div>

                            <div className="w-full h-auto">
                                <h1 className="text-sm -mb-1 font-light text-[#45044a]">
                                    Order ID :{" "}
                                </h1>
                                <span className="text-sm font-semibold text-[#45044a]">
                                    {transactionData.Tid}
                                </span>
                            </div>

                            <div className="w-full h-auto">
                                <h1 className="text-sm -mb-1 font-light text-[#45044a]">
                                    Customer :{" "}
                                </h1>
                                <span className="text-sm font-semibold text-[#45044a]">
                                    {transactionData.customer_name}
                                </span>
                            </div>

                            <div className="w-full h-auto">
                                <h1 className="text-sm -mb-1 font-light text-[#45044a]">
                                    Email :{" "}
                                </h1>
                                <span className="text-sm font-semibold text-[#45044a]">
                                    {transactionData.customer_email}
                                </span>
                            </div>
                        </div>

                        <div className="h-full flex flex-col gap-4">
                            <div className="w-full h-auto">
                                <h1 className="text-sm -mb-1 font-light text-[#45044a]">
                                    Metode pembayaran :{" "}
                                </h1>
                                <span className="text-sm font-semibold text-[#45044a]">
                                    {transactionData?.payment_method || "-"}
                                </span>
                            </div>

                            <div className="w-full h-auto">
                                <h1 className="text-sm -mb-1 font-light text-[#45044a]">
                                    Status pembayaran :{" "}
                                </h1>
                                <span className="text-sm font-semibold text-[#45044a]">
                                    {transactionStatus(transactionData.status)}
                                </span>
                            </div>

                            <div className="w-full h-auto">
                                <h1 className="text-sm -mb-1 font-light text-[#45044a]">
                                    Total pembayaran :{" "}
                                </h1>
                                <span className="text-sm font-semibold text-[#45044a]">
                                    {formatPriceToIDR(
                                        transactionData?.total || 0
                                    )}
                                </span>
                            </div>
                        </div>
                    </div>

                    <div className="w-full h-auto mt-6">
                        <button
                            onClick={() =>
                                transactionData?.status === "PAID"
                                    ? goBackHandler()
                                    : paymentHandler(transactionData.snap_token)
                            }
                            className="w-fit h-fit py-2 px-5 rounded-full bg-indigo-600 text-white text-sm"
                        >
                            {transactionData?.status === "PAID"
                                ? "Lakukan pembelian lain"
                                : "Bayar sekarang"}
                        </button>
                    </div>
                </>
            ) : null}
        </>
    );
}
