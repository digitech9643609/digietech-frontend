import Link from "next/link";

export default function SideBarInformation({}) {
    return (
        <div className="w-full h-auto flex flex-col gap-4">
            <div className="w-full h-auto flex flex-col gap-4 px-5 py-4 bg-white bg-opacity-15 rounded-lg">
                <span className="text-white text-sm font-light">
                    Halaman ini akan otomatis diperbarui setelah pembayaran Anda
                    selesai.
                </span>

                <span className="text-white text-sm font-light">
                    Anda juga dapat mengecek status pembayaran secara manual
                    dengan mengeklik tombol di bawah
                </span>

                <div className="w-full flex items-center justify-center">
                    <button className="w-fit h-10 px-4 bg-yellow-500 rounded-full">
                        cek status pembayaran
                    </button>
                </div>
            </div>

            <div className="w-full h-auto px-5 py-4 bg-slate-50 rounded-lg">
                <h1 className="text-base text-[#45044a] font-bold">
                    Selesaikan pembayaran anda
                </h1>

                <p className="text-sm text-[#45044a] mt-2">
                    Selesaikan pembayaran Anda dengan mengikuti instruksi yang
                    sudah dikirim. Setelah pembayaran Anda di konfirmasi, bukti
                    order anda akan dikirim langsung ke . Apabila anda mempunyai
                    pertanyaan, silahkan hubungi{" "}
                    <Link href="#" className="text-blue-600 underlined">
                        Pusat Bantuan
                    </Link>{" "}
                    kami.
                </p>
            </div>

            <div className="w-full h-auto flex flex-col gap-2 px-5 py-4 bg-white bg-opacity-15 rounded-lg">
                <h1 className="text-xl font-semibold text-slate-50 mb-2">
                    Butuh bantuan?
                </h1>

                <Link href="#" className="text-sm text-yellow-400">
                    Masalah Pembayaran & pembelian
                </Link>
                <Link href="#" className="text-sm text-yellow-400">
                    Pengumuman Lainnya
                </Link>
                <Link href="#" className="text-sm text-yellow-400">
                    Lainnya
                </Link>
            </div>
        </div>
    );
}
