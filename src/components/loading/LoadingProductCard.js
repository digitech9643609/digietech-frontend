export default function LoadingProductCard({ numberOfCard }) {
    return (
        <>
            {[...Array(numberOfCard)].map((_, index) => {
                return (
                    <div
                        key={index}
                        className="h-40 border p-2 rounded-xl bg-zinc-200 animate-pulse"
                    ></div>
                );
            })}
        </>
    );
}
