import HamburgerMenuIcon from "./icon/HamburgerMenuIcon";
import SearchIcon from "./icon/SearchIcon";

export default function NavigationBar({}) {
  return (
    <div className="w-full h-16 flex justify-center">
      <div className="w-2/3 h-full flex justify-between">
        <div className="w-auto h-full flex items-center gap-1">
          <button className="w-9 h-9 flex items-center justify-center rounded-md">
            <HamburgerMenuIcon width={8} height={8} lineColor={"#ffffff"} />
          </button>

          <h1 className="text-sm text-white italic font-light truncate">
            Website top-up terbesar, tercepat dan terpercaya yeay😉
          </h1>
        </div>

        <div className="w-auto h-full flex items-center gap-3">
          <div className="h-10 w-80 py-[2px] px-2 flex items-center gap-1 bg-slate-100 rounded-md">
            <input
              type="text"
              className="flex-1 h-full bg-transparent focus:outline-none placeholder:text-sm placeholder:text-slate-500"
              placeholder="search..."
            />

            <button className="w-9 h-9 flex items-center justify-center rounded-md">
              <SearchIcon width={7} height={7} lineColor={"#4f46e5"} />
            </button>
          </div>

          <div className="w-fit h-full flex items-center">
            <button className="w-fit h-fit py-[6px] px-6 rounded-full capitalize text-sm text-white bg-indigo-600">
              Daftar
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
