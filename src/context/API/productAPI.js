import axios from "axios";
import useSWR from "swr";

const productAPI = axios.create({
  baseURL: process.env.NEXT_PUBLIC_BE_BASE_URL,
});

const fetcher = async (url) => {
  try {
    const response = await productAPI.get(url);

    return response.data;
  } catch (error) {
    console.log(error);
  }
};

/* GET ALL PRODUCTS */
export const useGetProducts = () => {
  const { data, isLoading, error } = useSWR(
    `${process.env.NEXT_PUBLIC_BE_BASE_URL}/product/all`,
    fetcher
  );

  return {
    response: data,
    products: data?.data,
    productsLoading: isLoading,
    productsError: error,
  };
};
