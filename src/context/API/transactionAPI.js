import axios from "axios";
import useSWR from "swr";

const transactionAPI = axios.create({
    baseURL: process.env.NEXT_PUBLIC_BE_BASE_URL,
});

const fetcher = async (url) => {
    try {
        const response = await transactionAPI.get(url);

        return response.data;
    } catch (error) {
        return console.log("error", error.message);
    }
};

/* GET TRANSACTION BY ID */
export const useGetTransactionByIdAPI = (transaction_id) => {
    const { data, isLoading, error } = useSWR(
        `${process.env.NEXT_PUBLIC_BE_BASE_URL}/transaction/by-id?transaction_id=${transaction_id}`,
        fetcher
    );

    return {
        response: data,
        data: data?.data,
        transactionLoading: isLoading,
        transactionError: error,
    };
};

/* CREATE NEW TRANSACTION */
export const createTransactionAPI = async (transactionData) => {
    try {
        const response = await transactionAPI.post(
            "/transaction/add",
            JSON.stringify(transactionData),
            {
                headers: {
                    "Content-Type": "application/json",
                },
            }
        );

        return response.data;
    } catch (error) {
        return console.log("error", error.message);
    }
};
