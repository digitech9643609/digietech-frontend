const transactionStatus = (status) => {
    if (status === "PENDING_PAYMENT") return "Menunggu pembayaran";

    if (status === "PAID") return "Sudah dibayar";
    
    if (status === "CANCELED") return "Pembayaran dibatalkan";

    return "-"
};

module.exports = transactionStatus