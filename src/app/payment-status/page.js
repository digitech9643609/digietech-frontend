"use client";

import TransactionDetails from "@/components/TransactionDetails";
import useSnap from "@/hooks/useSnap";
import { useRouter } from "next/navigation";
import { Suspense, useEffect } from "react";

export default function OrderPage({}) {
    const { snapPopUp } = useSnap();
    const router = useRouter();

    useEffect(() => {
        const snapScript = `${process.env.NEXT_PUBLIC_MIDTRANS_API_URL}/snap/snap.js`;
        const myMidtransClientKey = process.env.NEXT_PUBLIC_MIDTRANS_CLIENT_KEY;

        const script = document.createElement("script");
        script.src = snapScript;
        script.setAttribute("data-client-key", myMidtransClientKey);

        document.body.appendChild(script);

        return () => {
            document.body.removeChild(script);
        };
    }, []);

    const paymentHandler = (snap_token) => {
        snapPopUp(snap_token, {
            onSuccess: function (result) {
                router.refresh();
            },
            onPending: function (result) {
                router.refresh();
            },
            onClose: function () {
                router.refresh();
            },
        });
    };

    const goToMainMenu = () => {
        router.push("/mobile-legends");
    };

    return (
        <div className="w-full h-auto px-5 py-6 rounded-lg bg-slate-50">
            <h1 className="text-2xl text-[#45044a] font-bold">
                Ringkasan pesanan
            </h1>

            <Suspense fallback={<div>Loading...</div>}>
                <TransactionDetails
                    paymentHandler={paymentHandler}
                    goBackHandler={goToMainMenu}
                />
            </Suspense>
        </div>
    );
}
