import NavigationBar from "../../components/NavigationBar";
import SideBarComponent from "../../components/SideBarComponent";

export default function MobileLegendsLayout({ children }) {
  return (
    <section className="w-full h-auto min-h-screen bg-[#260229] flex flex-col gap-5 items-center">
      <nav className="w-full h-auto bg-[#45044a]">
        <NavigationBar />
      </nav>

      <section className="w-2/3 h-auto flex flex-wrap gap-5 pb-10">
        <aside className="w-1/3 h-auto">
          <SideBarComponent />
        </aside>

        <main className="flex-1 h-auto">{children}</main>
      </section>
    </section>
  );
}
