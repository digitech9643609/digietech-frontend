"use client";
import LoadingProductCard from "@/components/loading/LoadingProductCard";
import { useGetProducts } from "@/context/API/productAPI";
import { createTransactionAPI } from "@/context/API/transactionAPI";
import useSnap from "@/hooks/useSnap";
import { formatPriceToIDR } from "@/utils/formatIDRCurrency";
import Image from "next/image";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";

export default function MobileLegendsPage({}) {
    const categories = [
        { id: 1, title: "diamond", imgURL: "/logo/diamond.png" },
        {
            id: 2,
            title: "weekly diamond pass",
            imgURL: "/logo/weekly-diamond-pass.png",
        },
        {
            id: 3,
            title: "twilight pass",
            imgURL: "/logo/twilight-pass.png",
            custom_width: "100",
            custom_height: "80",
        },
    ];

    const { snapEmbed, snapPopUp } = useSnap();
    const router = useRouter();

    const [selectedCategory, setSelectedCategory] = useState(1);
    const [selectedProduct, setSelectedProduct] = useState();
    const [invalidMandatoryData, setInvalidMandatoryData] = useState(false);

    const [transactionData, setTransactionData] = useState({
        customer_name: "",
        customer_email: "",
        products: [],
        price: 0,
        product_name: "",
        userGameId: null,
        serverGameId: null,
    });

    /* FETCH API */
    const { response, products, productsError, productsLoading } =
        useGetProducts();

    const selectCategoryHandler = (category_id) => {
        setSelectedCategory(category_id);
    };

    const selectProductHandler = (product) => {
        setSelectedProduct(product.id);
        setTransactionData((prevData) => {
            return {
                ...prevData,
                products: [{ id: product.id, quantity: 1 }],
                price: product.price,
                product_name: product.name,
            };
        });
    };

    const onChangeHandler = (e) => {
        const { name, value } = e.target;

        setTransactionData((prevData) => {
            return {
                ...prevData,
                [name]: value,
            };
        });
    };

    const createTransactionHandler = async () => {
        try {
            const response = await createTransactionAPI(transactionData);

            if (response && response.status === "success") {
                snapPopUp(response.data.snap_token, {
                    onSuccess: function (result) {
                        router.push(
                            `/payment-status?order_id=${response.data.id}`
                        );
                    },
                    onPending: function (result) {
                        router.push(
                            `/payment-status?order_id=${response.data.id}`
                        );
                    },
                    onClose: function () {
                        router.push(
                            `/payment-status?order_id=${response.data.id}`
                        );
                    },
                });
            } else {
                // console.log("response", response);
                alert("Transaction failed!");
            }
        } catch (error) {
            alert("Transaction failed!");
        }
    };

    const validateEmail = (email) => {
        // Regular expression for email validation
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return regex.test(email);
    };

    useEffect(() => {
        if (
            !selectedProduct ||
            !transactionData.customer_email ||
            !validateEmail(transactionData.customer_email) ||
            !transactionData.customer_name ||
            !transactionData.serverGameId ||
            !transactionData.userGameId
        ) {
            setInvalidMandatoryData(true);
        } else {
            setInvalidMandatoryData(false);
        }
    }, [selectedProduct, transactionData]);

    return (
        <section className="w-full h-auto flex flex-col gap-5 pb-10">
            <div className="w-full h-auto flex flex-col gap-6 pt-3 bg-slate-50 rounded-lg overflow-hidden">
                <div className="w-full h-10 flex items-center gap-2 px-5">
                    <div className="w-10 h-10 bg-indigo-600 flex items-center justify-center rounded-md">
                        <span className="font-bold text-white">1</span>
                    </div>
                    <h1 className="text-xl font-bold text-slate-800">
                        Masukkan user ID
                    </h1>
                </div>

                <div className="w-full h-auto flex items-center gap-2 px-5">
                    <div className="w-52 h-12 px-3 border border-zinc-200 flex items-center rounded-md">
                        <input
                            type="text"
                            className="w-full h-full text-center bg-transparent focus:outline-none placeholder:text-sm placeholder:text-slate-500"
                            placeholder="masukkan user ID*"
                            name="userGameId"
                            onChange={onChangeHandler}
                            autoFocus
                        />
                    </div>

                    <div className="w-52 h-12 px-3 border border-zinc-200 flex items-center rounded-md">
                        <span>(</span>
                        <input
                            type="text"
                            className="w-full h-full text-center bg-transparent focus:outline-none placeholder:text-sm placeholder:text-slate-500"
                            placeholder="zone ID*"
                            name="serverGameId"
                            onChange={onChangeHandler}
                        />
                        <span>)</span>
                    </div>
                </div>

                <div className="w-full h-auto px-5">
                    <span className="text-[10px] text-slate-500 italic">
                        Untuk mengetahui User ID Anda, silakan klik menu profile
                        dibagian kiri atas pada menu utama game. User ID akan
                        terlihat dibagian bawah Nama Karakter Game Anda. Silakan
                        masukkan User ID Anda untuk menyelesaikan transaksi.
                        Contoh : 12345678(1234).
                    </span>
                </div>

                <div className="w-full h-12 bg-indigo-200 flex items-center gap-2 px-4">
                    <input type="checkbox" id="send-gift" />
                    <label
                        htmlFor="send-gift"
                        className="text-xs font-semibold"
                    >
                        Kirim item ini sebagai hadiah ke teman anda
                    </label>
                </div>
            </div>

            <div className="w-full h-auto flex flex-col gap-6 py-3 pb-4 bg-slate-50 rounded-lg overflow-hidden">
                <div className="w-full h-10 flex items-center gap-2 px-5">
                    <div className="w-10 h-10 bg-indigo-600 flex items-center justify-center rounded-md">
                        <span className="font-bold text-white">2</span>
                    </div>
                    <h1 className="text-xl font-bold text-slate-800">
                        Pilih nominal top up
                    </h1>
                </div>

                <div className="w-full h-auto flex flex-col gap-4 px-5">
                    {/* CATEGORIES */}
                    <div className="w-full h-auto flex flex-col gap-2">
                        <h1 className="text-base font-semibold">
                            Pilih kategori
                        </h1>

                        <div className="w-full h-auto grid grid-cols-6 gap-2">
                            {productsLoading ? (
                                <LoadingProductCard numberOfCard={3} />
                            ) : (
                                categories.map((category, index) => {
                                    return (
                                        <button
                                            key={index}
                                            onClick={() =>
                                                selectCategoryHandler(
                                                    category.id
                                                )
                                            }
                                            className={`h-40 border p-2 rounded-xl flex flex-col gap-3 justify-center items-center ${
                                                selectedCategory === category.id
                                                    ? "border-indigo-600 bg-indigo-100"
                                                    : "border-slate-200"
                                            }`}
                                        >
                                            <Image
                                                alt="img"
                                                src={category.imgURL}
                                                width={
                                                    category?.custom_width || 50
                                                }
                                                height={
                                                    category?.custom_height ||
                                                    50
                                                }
                                            />
                                            <span className="text-sm text-center font-semibold capitalize">
                                                {category.title}
                                            </span>
                                        </button>
                                    );
                                })
                            )}
                        </div>
                    </div>

                    {/* PRODUCTS */}
                    <div className="w-full h-auto flex flex-col gap-2">
                        <h1 className="text-base font-semibold">Pilih item</h1>

                        {!productsLoading && products?.length <= 0 ? (
                            <div className="w-full h-40 flex justify-center items-center bg-slate-100">
                                <span className="text-sm text-[#45044a] font-semibold">
                                    Products is empty
                                </span>
                            </div>
                        ) : !productsLoading && productsError ? (
                            <div className="w-full h-40 flex justify-center items-center bg-slate-100">
                                <span className="text-sm text-[#45044a] font-semibold">
                                    Oops, failed to load the products😕
                                </span>
                            </div>
                        ) : null}

                        <div className="w-full h-auto grid grid-cols-5 gap-2">
                            {productsLoading ? (
                                <LoadingProductCard numberOfCard={12} />
                            ) : (
                                products &&
                                products.map((product, index) => {
                                    return (
                                        <button
                                            key={index}
                                            onClick={() =>
                                                selectProductHandler(product)
                                            }
                                            className={`h-48 border p-2 rounded-xl flex flex-col gap-2 items-center justify-center ${
                                                selectedProduct === product.id
                                                    ? "border-indigo-600 bg-indigo-100"
                                                    : "border-slate-200"
                                            }`}
                                        >
                                            <Image
                                                alt="img"
                                                src="/logo/diamond.png"
                                                width={40}
                                                height={40}
                                            />
                                            <div className="w-full h-auto flex flex-col justify-center gap-1">
                                                <span className="text-sm text-center font-semibold capitalize">
                                                    {product.name}
                                                </span>
                                                <span className="text-xs text-center font-normal capitalize">
                                                    {`${formatPriceToIDR(
                                                        product.price || 0
                                                    )}`}
                                                </span>
                                            </div>
                                        </button>
                                    );
                                })
                            )}
                        </div>
                    </div>
                </div>
            </div>

            <div className="w-full h-auto flex flex-col gap-6 pt-3 bg-slate-50 rounded-lg overflow-hidden">
                <div className="w-full h-10 flex items-center gap-2 px-5">
                    <div className="w-10 h-10 bg-indigo-600 flex items-center justify-center rounded-md">
                        <span className="font-bold text-white">3</span>
                    </div>
                    <h1 className="text-xl font-bold text-slate-800">Beli</h1>
                </div>

                <div className="w-full h-auto flex flex-col justify-center gap-2 px-5">
                    <h1 className="text-sm font-medium">
                        Masukkan email untuk mendapatkan bukti pembayaran atas
                        pembelian anda, harap mengisi alamat emailnya Digitech
                        Rewards sebesar 2% yang terkirim akan di notify ke email
                        kamu
                    </h1>

                    <div className="w-full h-12 px-3 border border-zinc-200 flex items-center rounded-md">
                        <input
                            type="text"
                            name="customer_name"
                            className="w-full h-full bg-transparent focus:outline-none placeholder:text-sm placeholder:text-slate-500"
                            placeholder="nama*"
                            onChange={onChangeHandler}
                        />
                    </div>

                    <div className="w-full h-12 px-3 border border-zinc-200 flex items-center rounded-md">
                        <input
                            type="email"
                            name="customer_email"
                            className="w-full h-full bg-transparent focus:outline-none placeholder:text-sm placeholder:text-slate-500 invalid:text-rose-600"
                            placeholder="alamat email*"
                            onChange={onChangeHandler}
                        />
                    </div>
                </div>

                <div className="w-full h-32 py-5 px-6 bg-indigo-100 flex items-center gap-2">
                    <div className="w-2/3 h-full flex flex-col gap-1">
                        <span className="text-lg font-semibold text-green-600">
                            {formatPriceToIDR(transactionData.price)}
                        </span>

                        {transactionData?.product_name ? (
                            <span className="text-sm">
                                ({transactionData.product_name})
                            </span>
                        ) : null}
                    </div>
                    <div className="w-1/3 h-full  flex justify-center items-center">
                        <button
                            disabled={invalidMandatoryData}
                            onClick={createTransactionHandler}
                            className={`w-full h-10 rounded-lg  text-white font-bold ${
                                invalidMandatoryData
                                    ? "bg-indigo-500 opacity-80"
                                    : "bg-indigo-600"
                            }`}
                        >
                            Bayar sekarang
                        </button>
                    </div>
                </div>
            </div>
        </section>
    );
}
